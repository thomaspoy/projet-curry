const express = require('express');
const bodyParser = require("body-parser");
const methodOverride = require('method-override');
const session = require('express-session')
const csurf = require('csurf');

const app = express();

const dbConfig = require('./data/db.json');

const Database = require('./core/Database');
const Controller = require('./core/Controller');
const AuthManager = require('./core/AuthManager');

let db = new Database(dbConfig.host, dbConfig.port, dbConfig.user, dbConfig.password, dbConfig.database);
const { Pompier, Caserne, User, Role } = db.sequelize.models;

AuthManager.userModel = User;
AuthManager.roleModel = Role;

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride('_method'));
app.use(session({ secret: 'superSecret', path: '/' }));
app.use(csurf({ cookie: false }));

app.get('/', (req, res) => Controller.showHomepage(req, res));
app.get('/login', (req, res) => Controller.showLogin(req, res));

app.get('/logout', (req, res) => Controller.handleLogout(req, res));
app.post('/login', (req, res) => Controller.handleLogin(req, res, User));
app.get('/unauthorized', (req, res) => Controller.showUnauthorized(req, res));

app.get('/pompiers', (req, res) => Controller.list(req, res, "Liste des pompiers", "pompier", Pompier, "pompiers"));
app.get('/casernes', (req, res) => Controller.list(req, res, "Liste des casernes", "caserne", Caserne, "casernes"));
app.get('/users', (req, res) => Controller.list(req, res, "Liste des utilisateurs", "user", User, "manage"));

app.get('/pompiers/:identifier', (res, req) => Controller.showDetails(res, req, 'Matricule', '/pompiers', 'pompier', Pompier, db, "pompiers"));
app.get('/casernes/:identifier', (res, req) => Controller.showDetails(res, req, 'NumCaserne', '/casernes', 'caserne', Caserne, db, "casernes"));
app.get('/users/:identifier', (res, req) => Controller.showDetails(res, req, 'id', '/users', 'user', User, db, "manage"));

app.get('/new/pompier', (req, res) => Controller.showCreate(req, res, 'Créer un nouveau pompier ', '/pompiers', 'pompier', db, "pompiers"));
app.get('/new/caserne', (req, res) => Controller.showCreate(req, res, 'Créer une nouvelle caserne', '/casernes', 'caserne', db, "casernes"));
app.get('/new/user', (req, res) => Controller.showCreate(req, res, 'Créer un nouvel utilisateur', '/users', 'user', db, "manage"));

app.post('/pompiers/:identifier', (res, req) => Controller.handleModifications(res, req, 'Matricule', '/pompiers', 'pompier', Pompier, db, "pompiers"));
app.post('/casernes/:identifier', (res, req) => Controller.handleModifications(res, req, 'NumCaserne', '/casernes', 'caserne', Caserne, db, "casernes"));
app.post('/users/:identifier', (res, req) => Controller.handleModifications(res, req, 'id', '/users', 'user', User, db, "manage"));

app.delete('/pompiers/:identifier', (res, req) => Controller.handleDelete(res, req, 'Matricule', '/pompiers', 'pompier', Pompier, "pompiers"));
app.delete('/casernes/:identifier', (res, req) => Controller.handleDelete(res, req, 'NumCaserne', '/casernes', 'caserne', Caserne, "casernes"));
app.delete('/users/:identifier', (res, req) => Controller.handleDelete(res, req, 'id', '/users', 'user', User, "manage"));

app.post('/new/pompier', (res, req) => Controller.handleCreate(res, req, '/pompiers', 'pompier', Pompier, "pompiers"));
app.post('/new/caserne', (res, req) => Controller.handleCreate(res, req, '/casernes', 'caserne', Caserne, "casernes"));
app.post('/new/user', (res, req) => Controller.handleCreate(res, req, '/users', 'user', User, "manage"));

app.get('/new/error', (res, req) => Controller.showCreateError(res, req, "manage"));

app.listen(3000);