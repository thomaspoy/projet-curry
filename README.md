<img src="https://i.ibb.co/gTx98pz/curry.png">

## 🔧 Configuration

Les paramètres de connexion à la base de données sont dans le fichier `data/db.json` :

```json
{
    "host": "localhost",
    "port": 3306,
    "user": "root",
    "password": "",
    "database": "pompiers"
}
```

Le fichier `data/rows.json` contient les différentes informations sur les différents champs pour chaque entité :

```json
{
    "nom_entité": {
        "nom_colonne": {
          "displayName": "Nom du champ", // Nom affiché
          "type": "text", // Type de l'input
          "isPrimaryKey": true, // La colonne est une clé primaire
          "isForeignKey": true, // La colonne est une clé étrangère
          "references": "pompiers", // Quelle est la table qu'il réfère (isForeignKey = true)
          "referenceColumn": "CodeGrade", // Quelle colonne réfère t-il dans cette table (isForeignKey = true)
          "referenceDisplayColumn": "NomGrade", // Quelle colonne de cette table est la valeur à afficher (isForeignKey = true)
          "maxlength": 7, // Taille maximale du champ
          "minlength": 1, // Taille minimale du champ
          "showLink": true, // Afficher le lien vers les détails (isPrimaryKey = true | isForeignKey = true)
          "pattern": "Ma[0-9]+" // Pattern à respecter pour la valeur
        }
    }
}
```


## 🔨 Classes utilitaires
- [Controller](core/Controller.js) - Affiche les vues avec les données qu'elles nécessitent et effectue des modifications sur la base de données.
- [Database](core/Database.js) - S'occupe du lien avec la base de données et définit les modèles Pompier, Caserne, User et Role.
- [AuthManager](core/AuthManager.js) - Gère l'authentification et le stockage des données relatives aux utilisateurs dans les sessions.

## 📷 Screenshots

### Page de connexion
<a href="https://ibb.co/3yvG6wR"><img src="https://i.ibb.co/N6rQDXT/Screenshot-2021-11-20-at-17-39-16-Curry.png" alt="Screenshot-2021-11-20-at-17-39-16-Curry" border="0"></a>

### Liste des pompiers
<a href="https://ibb.co/zP9GDpW"><img src="https://i.ibb.co/QYSrtL7/Screenshot-2021-11-20-at-17-35-46-Curry.png" alt="Screenshot-2021-11-20-at-17-35-46-Curry" border="0"></a>

### Détails d'un pompier
<a href="https://ibb.co/5vmzKvS"><img src="https://i.ibb.co/rMYLdMz/Screenshot-2021-11-20-at-17-36-04-Curry.png" alt="Screenshot-2021-11-20-at-17-36-04-Curry" border="0"></a>

### Création d'un nouveau pompier
<a href="https://ibb.co/0p0JD92"><img src="https://i.ibb.co/FkQ7xY3/Screenshot-2021-11-20-at-17-35-55-Curry.png" alt="Screenshot-2021-11-20-at-17-35-55-Curry" border="0"></a>