const {
    Sequelize,
    DataTypes,
    QueryTypes
} = require('sequelize');

class Database {
    constructor(host, port, user, password, database) {
        this.sequelize = new Sequelize(database, user, password, {
            host: host,
            port: port,
            dialect: 'mysql',
            charset: 'utf8'
        });

        this.registerModels();
    }

    registerModels() {
        this.sequelize.define("Pompier", {
            Matricule: {
                type: DataTypes.STRING,
                primaryKey: true,
                allowNull: false,
            },
            Prenom: {
                type: DataTypes.STRING,
                allowNull: false
            },
            Nom: {
                type: DataTypes.STRING,
                allowNull: false
            },
            ChefAgret: {
                type: DataTypes.STRING,
                allowNull: false
            },
            DateNaissance: {
                type: DataTypes.DATE,
                allowNull: false
            },
            NumCaserne: {
                type: DataTypes.INTEGER,
                references: 'casernes',
                referencesKey: 'NumCaserne',
                allowNull: false
            },
            CodeGrade: {
                type: DataTypes.STRING,
                allowNull: false
            },
            matriculeRespo: {
                type: DataTypes.STRING,
                references: 'pompiers',
                referencesKey: 'Matricule',
                allowNull: false
            }
        }, {
            tableName: 'pompiers',
            timestamps: false
        });

        this.sequelize.define("Caserne", {
            NumCaserne: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            Adresse: {
                type: DataTypes.STRING,
                allowNull: false
            },
            CP: {
                type: DataTypes.STRING,
                allowNull: false
            },
            Ville: {
                type: DataTypes.STRING,
                allowNull: false
            },
            CodeTypeC: {
                type: DataTypes.INTEGER,
                references: 'typecasernes',
                referencesKey: 'CodeTypeC',
                allowNull: false
            }
        }, {
            tableName: 'casernes',
            timestamps: false
        });
        
        this.sequelize.define("User", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            role: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: 'roles',
                referencesKey: 'id'
            }
        }, {
            tableName: 'users',
            timestamps: false
        });

        this.sequelize.define("Role", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            pl_pompiers: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            pl_casernes: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            pl_manage: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            tableName: 'roles',
            timestamps: false
        });
    }

    selectIdDisplay(table, identifierCol, displayCol) {
        return new Promise(async (rs, rj) => {
            var result = await this.sequelize.query('SELECT ' + identifierCol + ', ' + displayCol + ' FROM ' + table, { type: QueryTypes.SELECT });

            var res = [];
            
            for(let i = 0; i < result.length; i++) {
                res.push({
                    id: result[i][identifierCol],
                    display: result[i][displayCol]
                })
            }

            rs(res);
        });
    }

}

module.exports = Database;