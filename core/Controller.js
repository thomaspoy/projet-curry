const rows = require('../data/rows.json');
const sha512 = require('js-sha512').sha512;

const AuthManager = require('./AuthManager');

class Controller {

    static async showHomepage(req, res) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');

        res.render('home', {
            user: req.session.user
        });
    }

    static async showUnauthorized(req, res) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');

        res.render('unauthorized', {
            user: req.session.user
        });
    }

    static async showLogin(req, res) {
        if (await AuthManager.isLoggedIn(req)) return res.redirect('/');

        res.render('login', {
            csrfToken: req.csrfToken(),
            error: false,
            success: false
        });
    }

    static async handleLogin(req, res, userModel) {
        var user = await userModel.findOne({
            where: {
                email: req.body.email,
                password: sha512(req.body.password)
            }
        });

        if (user) {
            req.session.userId = user.id;

            res.render('login', {
                csrfToken: req.csrfToken(),
                error: false,
                success: "Redirection dans 3s...",
                user: req.session.user
            });
        } else {
            res.render('login', {
                csrfToken: req.csrfToken(),
                error: "Votre mot de passe ou adresse e-mail est incorrect, faites attention les champs sont sensibles aux majuscules et minuscules.",
                success: false,
                user: req.session.user
            });
        }
    }

    static async handleLogout(req, res) {
        AuthManager.logout(req);
        res.redirect('/login');
    }

    static async list(req, res, pageName, entityName, entityModel, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].select) return res.redirect('/unauthorized');

        var data = await entityModel.findAll();

        res.render('list', {
            pageName: pageName,
            rows: rows[entityName],
            data: data,
            entityName: entityName,
            user: req.session.user,
            note: false
        });
    }

    static async showDetails(req, res, identifierColumn, redirect, entityName, entityModel, database, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].select) return res.redirect('/unauthorized');

        var whereClauses = {};
        whereClauses[identifierColumn] = req.params.identifier;

        var data = await entityModel.findOne({
            where: whereClauses
        });

        var foreignKeys = {};

        for (let i = 0; i < Object.keys(rows[entityName]).length; i++) {
            var value = Object.values(rows[entityName])[i];
            var key = Object.keys(rows[entityName])[i];

            if (value.isForeignKey && value.references && value.referenceColumn && value.referenceDisplayColumn) {
                var dbData = await database.selectIdDisplay(value.references, value.referenceColumn, value.referenceDisplayColumn);
                foreignKeys[key] = dbData;
            }
        }

        if (data) {
            res.render('show', {
                data: data,
                rows: rows[entityName],
                note: false,
                csrfToken: req.csrfToken(),
                user: req.session.user,
                foreignKeys: foreignKeys
            });
        } else {
            res.redirect(redirect);
        }
    }

    static async showCreate(req, res, pageName, redirect, entityName, database, permissionType, note = false) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].insert) return res.redirect('/unauthorized');

        var foreignKeys = {};

        for (let i = 0; i < Object.keys(rows[entityName]).length; i++) {
            var value = Object.values(rows[entityName])[i];
            var key = Object.keys(rows[entityName])[i];

            if (value.isForeignKey && value.references && value.referenceColumn && value.referenceDisplayColumn) {
                var dbData = await database.selectIdDisplay(value.references, value.referenceColumn, value.referenceDisplayColumn);
                foreignKeys[key] = dbData;
            }
        }

        res.render('create', {
            pageName: pageName,
            rows: rows[entityName],
            redirectPath: redirect,
            csrfToken: req.csrfToken(),
            user: req.session.user,
            foreignKeys,
            note
        });
    }

    static async handleModifications(req, res, identifierColumn, redirect, entityName, entityModel, database, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].update) return res.redirect('/unauthorized');

        var whereClauses = {};
        whereClauses[identifierColumn] = req.params.identifier;

        var data = await entityModel.findOne({
            where: whereClauses
        });

        if (data) {
            var updatedData = {};
            var foreignKeys = {};

            for (let i = 0; i < Object.keys(rows[entityName]).length; i++) {
                var column = Object.keys(rows[entityName])[i];

                if (req.body[column]) {
                    if (rows[entityName][column].hashedValue) {
                        updatedData[column] = sha512(req.body[column]);
                    } else {
                        updatedData[column] = req.body[column];
                    }

                    if (rows[entityName][column].isForeignKey && rows[entityName][column].references && rows[entityName][column].referenceColumn && rows[entityName][column].referenceDisplayColumn) {
                        var dbData = await database.selectIdDisplay(rows[entityName][column].references, rows[entityName][column].referenceColumn, rows[entityName][column].referenceDisplayColumn);
                        foreignKeys[column] = dbData;
                    }
                }
            }

            await entityModel.update(updatedData, {
                where: whereClauses
            }).catch(err => {
                res.render('show', {
                    data: data,
                    rows: rows[entityName],
                    note: {
                        "type": "error",
                        "text": "<i class=\"las la-exclamation-circle\"></i> Impossible d'enregistrer les modifications, vérifiez les données envoyées."
                    },
                    csrfToken: req.csrfToken(),
                    user: req.session.user,
                    foreignKeys: foreignKeys
                });

                return;
            });

            data = await entityModel.findOne({
                where: whereClauses
            });

            res.render('show', {
                data: data,
                rows: rows[entityName],
                note: {
                    "type": "success",
                    "text": "<i class=\"las la-check\"></i> Modifications enregistrées avec succès !"
                },
                csrfToken: req.csrfToken(),
                user: req.session.user,
                foreignKeys: foreignKeys
            });
        } else {
            res.redirect(redirect);
        }
    }

    static async handleDelete(req, res, identifierColumn, redirect, entityName, entityModel, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].delete) return res.redirect('/unauthorized');

        var whereClauses = {};
        whereClauses[identifierColumn] = req.params.identifier;

        var data = await entityModel.findOne({
            where: whereClauses
        });

        if (data) {
            await entityModel.destroy({
                where: whereClauses
            });
        }

        res.redirect(redirect);
    }

    static async handleCreate(req, res, redirect, entityName, entityModel, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].insert) return res.redirect('/unauthorized');

        var createData = {};

        for (let i = 0; i < Object.keys(rows[entityName]).length; i++) {
            var column = Object.keys(rows[entityName])[i];

            if (req.body[column]) {
                if (rows[entityName][column].hashedValue) {
                    createData[column] = sha512(req.body[column]);
                } else {
                    createData[column] = req.body[column];
                }
            }
        }

        var success = true;

        await entityModel.create(createData).catch(err => {
            res.redirect('/new/error?lastUrl=/new/' + entityName);
            success = false;
            return;
        }); 

        if(success) { res.redirect(redirect); }
    }

    static async showCreateError(req, res, permissionType) {
        if (!(await AuthManager.isLoggedIn(req))) return res.redirect('/login');
        if (!req.session.user.permissions[permissionType].insert) return res.redirect('/unauthorized');

        res.render('error', {
            errorTitle: "Impossible de créer l'entité",
            errorText: "Vérifiez les données envoyées et réessayez.",
            user: req.session.user,
            lastUrl: req.query.lastUrl ? req.query.lastUrl : false
        });
    }

}

module.exports = Controller;