class AuthManager {

    static userModel = null;
    static roleModel = null;

    static permissions = {
        SELECT: 1 << 0,
        UPDATE: 1 << 1,
        INSERT: 1 << 2,
        DELETE: 1 << 3
    }

    static logout(req) {
        delete req.session.userId;
    }

    static isLoggedIn(req) {
        return new Promise(async (rs, rj) => {
            if(req.session.userId) {
                req.session.user = await this.getCurrentUser(req);
                rs(true);
            }
    
            rs(false);
        });
    }

    static async setLoggedInAs(req, userId) {
        req.session.userId = userId;
    }

    static async getCurrentUser(req) {
        return new Promise(async (rs, rj) => {
            var user = await this.userModel.findOne({ where: { id: req.session.userId } });
            var role = await this.roleModel.findOne({ where: { id: user.role } });
            req.session.role = role;
            user.role = role;
            user.permissions = this.parseUserPermissions(user.role);
            rs(user);
        });
    }

    static parseUserPermissions(role) {
        var perms = {
            pompiers: {
                select: false,
                update: false,
                insert: false,
                delete: false
            },
            casernes: {
                select: false,
                update: false,
                insert: false,
                delete: false
            },
            manage: {
                select: false,
                update: false,
                insert: false,
                delete: false
            }
        };

        for(let i = 0; i < Object.keys(perms).length; i++) {
            var permType = Object.keys(perms)[i];
            if(role.pl_pompiers & this.permissions.SELECT) perms[permType].select = true;
            if(role.pl_pompiers & this.permissions.UPDATE) perms[permType].update = true;
            if(role.pl_pompiers & this.permissions.INSERT) perms[permType].insert = true;
            if(role.pl_pompiers & this.permissions.DELETE) perms[permType].delete = true;
        }

        return perms;
    }

}

module.exports = AuthManager;